package cn.spream.jstudy.zookeeper;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.Test;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-8-26
 * Time: 下午4:09
 * To change this template use File | Settings | File Templates.
 */
public class TestZookeeper {

    private static final String connectString = "127.0.0.1:2181";
    private static final int sessionTimeout = 10 * 1000;
    private static ZooKeeper zooKeeper;

    static {
        try {
            zooKeeper = new ZooKeeper(connectString, sessionTimeout, new Watcher() {
                @Override
                public void process(WatchedEvent event) {
                    System.out.println("watched:" + event.getType());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void test(){
        testCreate();
        testExist();
        testGet();
        testSet();
        testGet();
        testDelete();
        testExist();
    }

    @Test
    public void testCreate(){
        try {
            /**
             * 创建一个给定的目录节点 path, 并给它设置数据，CreateMode 标识有四种形式的目录节点，
             * 分别是 PERSISTENT：持久化目录节点，这个目录节点存储的数据不会丢失；
             * PERSISTENT_SEQUENTIAL：顺序自动编号的目录节点，这种目录节点会根据当前已近存在的节点数自动加 1，然后返回给客户端已经成功创建的目录节点名；
             * EPHEMERAL：临时目录节点，一旦创建这个节点的客户端与服务器端口也就是 session 超时，这种节点会被自动删除；
             * EPHEMERAL_SEQUENTIAL：临时自动编号节点
             */
            String result = zooKeeper.create("/jstudy", "hello".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
            System.out.println("create:" + result);
        } catch (KeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testSet(){
        try {
            /**
             * 给 path 设置数据，可以指定这个数据的版本号，如果 version 为 -1 可以匹配任何版本
             */
            Stat stat = zooKeeper.setData("/jstudy", "world".getBytes(), -1);
            System.out.println("update:" + stat);
        } catch (KeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testGet(){
        try {
            /**
             * 获取这个 path 对应的目录节点存储的数据，数据的版本等信息可以通过 stat 来指定，同时还可以设置是否监控这个目录节点数据的状态
             */
            String result = new String(zooKeeper.getData("/jstudy", true, null));
            System.out.println("get:" + result);
        } catch (KeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testDelete(){
        try {
            /**
             * 删除 path 对应的目录节点，version 为 -1 可以匹配任何版本，也就删除了这个目录节点所有数据
             */
            zooKeeper.delete("/jstudy", -1);
            System.out.println("delete:成功");
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (KeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testExist(){
        try {
            /**
             * 判断某个 path 是否存在，并设置是否监控这个目录节点，这里的 watcher 是在创建 ZooKeeper 实例时指定的 watcher，
             * exists方法还有一个重载方法，可以指定特定的 watcher
             */
            Stat stat = zooKeeper.exists("/jstudy", true);
            System.out.println("exist:" + stat);
        } catch (KeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
