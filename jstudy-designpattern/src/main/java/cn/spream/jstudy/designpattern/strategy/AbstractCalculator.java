package cn.spream.jstudy.designpattern.strategy;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:32
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractCalculator implements Calculator {

    protected int[] split(String exp, String opt) {
        String array[] = exp.split(opt);
        int arrayInt[] = new int[2];
        arrayInt[0] = Integer.parseInt(array[0]);
        arrayInt[1] = Integer.parseInt(array[1]);
        return arrayInt;
    }

}
