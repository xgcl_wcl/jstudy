package cn.spream.jstudy.dquartz.config;

/**
 * zookeeper配置文件
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/3/11
 * Time: 下午10:49
 * To change this template use File | Settings | File Templates.
 */
public class ZkConfig {

    private String address;
    private String rootPath;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }
}
