package cn.spream.jstudy.designpattern.proxy;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午2:01
 * To change this template use File | Settings | File Templates.
 */
public class TestProxy {

    @Test
    public void test(){
        ProxyLog4j proxyLog4j = new ProxyLog4j();
        proxyLog4j.debug("decorator");
        proxyLog4j.info("decorator");
        proxyLog4j.error("decorator");
    }

}
