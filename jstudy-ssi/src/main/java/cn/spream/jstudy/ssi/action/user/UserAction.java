package cn.spream.jstudy.ssi.action.user;

import cn.spream.jstudy.ssi.action.BaseAction;
import cn.spream.jstudy.ssi.common.Result;
import cn.spream.jstudy.ssi.domain.user.User;
import cn.spream.jstudy.ssi.domain.user.UserQuery;
import cn.spream.jstudy.ssi.service.user.UserService;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-20
 * Time: 下午1:18
 * To change this template use File | Settings | File Templates.
 */
public class UserAction extends BaseAction {

    private UserQuery userQuery = new UserQuery();
    private User user;
    private UserService userService;

    public String list(){
        Result result = userService.list(userQuery, pageIndex, pageSize);
        toVm(result);
        return "list";
    }

    public String toUpdate(){
        Result result = new Result(false);
        User user = userService.getById(userQuery.getId());
        if(user != null){
            result.setSuccess(true);
            result.addDefaultModel("user", user);
        }
        toVm(result);
        return "update";
    }

    public String update(){
        Result result = new Result(false);

        boolean updated = userService.update(user);
        result.addDefaultModel("updated", updated);

        User userResult = userService.getById(user.getId());
        result.addDefaultModel("user", userResult);

        result.setSuccess(true);
        toVm(result);
        return "update";
    }
    
    public String toAdd(){
        return "add";
    }
    
    public String add(){
        Result result = new Result(false);

        boolean added = userService.add(user);
        result.addDefaultModel("added", added);

        User userResult = userService.getById(user.getId());
        result.addDefaultModel("user", userResult);

        result.setSuccess(true);
        toVm(result);
        return "addResult";
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserQuery getUserQuery() {
        return userQuery;
    }

    public void setUserQuery(UserQuery userQuery) {
        this.userQuery = userQuery;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
