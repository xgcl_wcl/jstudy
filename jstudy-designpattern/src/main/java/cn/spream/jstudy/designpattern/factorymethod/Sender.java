package cn.spream.jstudy.designpattern.factorymethod;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午1:48
 * To change this template use File | Settings | File Templates.
 */
public interface Sender {

    public boolean send(String info);

}
