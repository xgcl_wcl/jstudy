package cn.spream.jstudy.designpattern.visitor;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午3:59
 * To change this template use File | Settings | File Templates.
 */
public interface Service {

    public void accept(Visitor visitor);

}
