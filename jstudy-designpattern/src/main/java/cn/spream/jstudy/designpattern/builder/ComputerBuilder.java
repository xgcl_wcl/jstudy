package cn.spream.jstudy.designpattern.builder;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午3:59
 * To change this template use File | Settings | File Templates.
 */
public class ComputerBuilder {

    /**
     * 组装戴尔电脑
     *
     * @return
     */
    public Computer buildDellComputer() {
        Computer computer = new Computer();
        computer.setName("戴尔");
        computer.setCrate(new DellCrate());
        computer.setDisplay(new DellDisplay());
        computer.setKeyboard(new DellKeyboard());
        computer.setMouse(new DellMouse());
        return computer;
    }

    /**
     * 组装华硕电脑
     *
     * @return
     */
    public Computer buildAsusComputer() {
        Computer computer = new Computer();
        computer.setName("华硕");
        computer.setCrate(new DellCrate());
        computer.setDisplay(new DellDisplay());
        computer.setKeyboard(new DellKeyboard());
        computer.setMouse(new DellMouse());
        return computer;
    }

    /**
     * 组装电脑
     *
     * @return
     */
    public Computer buildAssembleComputer() {
        Computer computer = new Computer();
        computer.setName("组装");
        computer.setCrate(new AsusCrate());
        computer.setDisplay(new DellDisplay());
        computer.setKeyboard(new DellKeyboard());
        computer.setMouse(new DellMouse());
        return computer;
    }

}
