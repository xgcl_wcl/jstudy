package cn.spream.jstudy.designpattern.observer;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午4:37
 * To change this template use File | Settings | File Templates.
 */
public interface Watcher {

    public String getName();

    public void update(String str);

}
