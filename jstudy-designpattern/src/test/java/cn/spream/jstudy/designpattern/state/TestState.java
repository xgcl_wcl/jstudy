package cn.spream.jstudy.designpattern.state;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午3:30
 * To change this template use File | Settings | File Templates.
 */
public class TestState {

    @Test
    public void test(){
        State state = new State();
        Context context = new Context(state);
        state.setValue("start");
        context.run();
        state.setValue("stop");
        context.run();
    }

}
